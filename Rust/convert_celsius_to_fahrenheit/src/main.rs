// 相互转换摄氏与华氏温度
// 华氏度 ＝ 摄氏度×1.8+32
// 摄氏度 ＝ (华氏度-32)÷1.8

// The program fork for https://github.com/ZedG2/rust-examples

use std::io;

fn main() {

    println!("Convert Celsius to Fahrenheit.");
    println!("Please select one to enter a number in Fahreheit and Celsius.");
    println!("Please enter a key(Key F or f is Fahreheit, Key C or c is Celsius)");

    loop {
        let mut input_str = String::new();
        io::stdin()
            .read_line(&mut input_str)
            .expect("输入一个值，以F结尾代表华氏度，或者以C结尾为摄氏度。比如77F。");
        input_str = input_str.trim().to_string();
        let at = input_str.len() - 1;
        let (value, unit) = input_str.split_at(at);
        match &unit.to_uppercase()[..] {
            "F" => {
                let celsius = f2c(&value);
                println!("华氏度 {} 等同于摄氏度 {}", value, celsius);
            }
            "C" => {
                let fahrenheit = c2f(&value);
                println!("摄氏度 {} 等同于华氏度 {}", value, fahrenheit);
            }
            _ => {
                println!("输入一个值，以F结尾代表华氏度，或者以C结尾为摄氏度。比如77F。");
                continue;
            }
        }
        break;
    }
    

}

fn f2c(value: &str) -> f64 {
    let fahrenheit: f64 = value.parse().expect("error");
    5.0 / 9.0 * (fahrenheit - 32.0)
}

fn c2f(value: &str) -> f64 {
    let celsius: f64 = value.parse().expect("error");
    9.0 / 5.0 * celsius + 32.0
}
